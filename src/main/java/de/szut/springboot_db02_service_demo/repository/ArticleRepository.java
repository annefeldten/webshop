package de.szut.springboot_db02_service_demo.repository;

import de.szut.springboot_db02_service_demo.model.Article;
import de.szut.springboot_db02_service_demo.model.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ArticleRepository extends JpaRepository<Article, Long> {

    public List<Article> findAllBySupplier(Supplier supplier);

}
