package de.szut.springboot_db02_service_demo.request;

import lombok.Getter;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
public class ArticleRequest {

    @NotBlank(message = "Designation is mandatory.")
    @Size(max = 50, message = "Designation must not have more than 50 characters.")
    private String designation;

    @NotNull(message = "Price is mandatory.")
    @DecimalMin("0.0")
    private double price;

    private long supplierId;

}
