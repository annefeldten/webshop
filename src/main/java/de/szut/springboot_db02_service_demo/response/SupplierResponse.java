package de.szut.springboot_db02_service_demo.response;


import lombok.Data;

@Data
public class SupplierResponse {

    private long id;
    private String name;
    private String phone;
    private String email;

}
