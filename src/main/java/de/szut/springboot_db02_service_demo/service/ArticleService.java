package de.szut.springboot_db02_service_demo.service;

import de.szut.springboot_db02_service_demo.exception.EntityNotFoundException;
import de.szut.springboot_db02_service_demo.model.Article;
import de.szut.springboot_db02_service_demo.model.Supplier;
import de.szut.springboot_db02_service_demo.repository.ArticleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ArticleService {

    private String exceptionMessage = "No article found.";

    private final ArticleRepository repository;

    /**
     * Erstellt einen neue Artikel.
     * @param article   Der zu erstellende Artikel.
     * @return  Der erstellte Artikel.
     */
    public Article create(Article article) {
        return repository.save(article);
    }

    /**
     * Liest einen Artikel auf Basis seiner ID.
     * @param id    Die ID.
     * @return  Der Artikel.
     */
    public Article readById(long id) {
        Optional<Article> oArticle = repository.findById(id);
        if (oArticle.isEmpty()) {
            throw new EntityNotFoundException(exceptionMessage + " id=" + id);
        }
        return oArticle.get();
    }

    /**
     * Liest den Lieferanten eines Artikels.
     * @param id    Die ID des Artikels.
     * @return  Der Lieferant.
     */
    public Supplier readForOneArticleItsSupplier(long id) {
        Article article = readById(id);
        return article.getSupplier();
    }

    /**
     * Liest alle Artikel.
     * @return  Die Liste aller Artikel.
     */
    public List<Article> readAll() {
        List<Article> articleList = repository.findAll();
        if (articleList.isEmpty()) {
            throw new EntityNotFoundException(exceptionMessage);
        }
        return articleList;
    }

    /**
     * Aktualisiert einen Artikel.
     * @param article   Der zu aktualisierende Artikel.
     * @return  Der aktualisierte Artikel.
     */
    public Article update(Article article) {
        Article serializedArticle = readById(article.getId());
        serializedArticle.setDesignation(article.getDesignation());
        serializedArticle.setPrice(article.getPrice());
        serializedArticle.setSupplier(article.getSupplier());
        return repository.save(serializedArticle);
    }

    /**
     * Löscht einen Artikel.
     * @param id    Die ID des zu löschenden Artikels.
     */
    public void delete(long id) {
        if (!repository.existsById(id)) {
            throw new EntityNotFoundException(exceptionMessage + " id=" + id);
        }
        repository.deleteById(id);
    }

}
