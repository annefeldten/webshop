package de.szut.springboot_db02_service_demo.service;

import de.szut.springboot_db02_service_demo.exception.EntityNotFoundException;
import de.szut.springboot_db02_service_demo.model.Article;
import de.szut.springboot_db02_service_demo.model.Supplier;
import de.szut.springboot_db02_service_demo.repository.SupplierRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SupplierService {

    private String exceptionMessage = "No supplier found.";

    private final SupplierRepository repository;

    /**
     * Erstellt einen neuen Lieferanten.
     * @param supplier   Der zu erstellende Lieferant.
     * @return  Der erstellte Lieferant.
     */
    public Supplier create(Supplier supplier) {
        return repository.save(supplier);
    }

    /**
     * Liest einen Lieferanten auf Basis seiner ID.
     * @param id    Die ID.
     * @return  Der Lieferant.
     */
    public Supplier readById(long id) {
        Optional<Supplier> oSupplier = repository.findById(id);
        if (oSupplier.isEmpty()) {
            throw new EntityNotFoundException(exceptionMessage + " id = " + id);
        }
        return oSupplier.get();
    }

    /**
     * Liest alle Lieferanten.
     * @return  Die Liste aller Lieferanten.
     */
    public List<Supplier> readAll() {
        List<Supplier> supplierList = repository.findAll();
        if (supplierList.isEmpty()) {
            throw new EntityNotFoundException(exceptionMessage);
        }
        return supplierList;
    }

    /**
     * Liest alle Artikel eines Lieferanten.
     * @param id    Die ID des Lieferanten.
     * @return  Die Liste aller Artikel.
     */
    public List<Article> readForOneSupplierAllArticles(long id) {
        Supplier supplier = readById(id);
        return supplier.getArticleList();
    }

    /**
     * Aktualisiert einen Lieferanten.
     * @param supplier   Der zu aktualisierende Lieferant.
     * @return  Der aktualisierte Lieferant.
     */
    public Supplier update(Supplier supplier) {
        Supplier serializedSupplier = readById(supplier.getId());
        serializedSupplier.setName(supplier.getName());
        serializedSupplier.setPhone(supplier.getPhone());
        serializedSupplier.setEmail(supplier.getEmail());
        serializedSupplier.setArticleList(supplier.getArticleList());
        return repository.save(serializedSupplier);
    }

    /**
     * Löscht einen Lieferanten auf Basis seiner ID.
     * @param id    Die ID.
     */
    public void delete(long id) {
        if (!repository.existsById(id)) {
            throw new EntityNotFoundException(exceptionMessage + " id = " + id);
        }
        repository.deleteById(id);
    }

}