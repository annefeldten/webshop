CREATE TABLE supplier (
    id IDENTITY NOT NULL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    phone VARCHAR(1024),
    email VARCHAR(1024) NOT NULL
);
CREATE TABLE article (
    id IDENTITY NOT NULL PRIMARY KEY,
    designation VARCHAR(50) NOT NULL,
    price DOUBLE NOT NULL,
    supplier_id INT REFERENCES supplier(id)
);

